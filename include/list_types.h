#include <sqlite3.h>
#include "sqlite3_client.h"
#include "fort.h"

static ft_table_t *table;
static int types_count;

static int callback_print_types_count(void *NotUsed, int argc, char **argv, char **azColName);

static int callback_print_types(void *NotUsed, int argc, char **argv, char **azColName);

void list_types();