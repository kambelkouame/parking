#include <sqlite3.h>
#include "sqlite3_client.h"

/**
 * Callback exécutée à la requête d'ajout
 * @param NotUsed void * 
 * @param int argc 
 * @param argv char**
 * @param azColName char**
 **/

int count_types;
int last_vehicle_id;

static int callback_save_vehicle(void *NotUsed, int argc, char **argv, char **azColName);

static int callback_save_parking(void *NotUsed, int argc, char **argv, char **azColName);

static int callback_list_types(void *NotUsed, int argc, char **argv, char **azColName);

static int callback_count_types(void *NotUsed, int argc, char **argv, char **azColName);

static int callback_get_last_vehicle_id(void *NotUsed, int argc, char **argv, char **azColName);

static int _list_types();

void add_vehicule();

int main();

