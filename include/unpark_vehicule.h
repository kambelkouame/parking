#include <sqlite3.h>
#include "sqlite3_client.h"

char parking_code[20];
unsigned int vehicle_id;
char date_arrivee[30];
char date_sortie[30];
unsigned int vehicle_count;
unsigned int parking_fees;
char matricule[15];
char client[160];
char libelle_type[100];
unsigned int tarif_heure;
unsigned int tarif_24;
unsigned int duree;

static int callback_unpark(void *NotUsed, int argc, char **argv, char **azColName);

static int callback_count_vehicle(void *NotUsed, int argc, char **argv, char **azColName);

static int callback_get_parked_vehicle(void *NotUsed, int argc, char **argv, char **azColName);

void unpark_vehicle(void);
