#include <sqlite3.h>
#include "sqlite3_client.h"
#include "fort.h"

static ft_table_t *table;
static int vehicles_count;

static int callback_print_vehicles_count(void *NotUsed, int argc, char **argv, char **azColName);

static int callback_print_vehicles(void *NotUsed, int argc, char **argv, char **azColName);

void list_vehicles();