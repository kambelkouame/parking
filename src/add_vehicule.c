#include <stdlib.h>
#include <stdio.h>
#include <sqlite3.h>
#include <string.h>
#include "../include/sqlite3_client.h"
#include "../include/add_vehicule.h"

#ifndef _ADD_VEHICULE_
#define _ADD_VEHICULE_

int count_types = 0;
int last_vehicle_id;

static int callback_save_vehicle(void *NotUsed, int argc, char **argv, char **azColName) {
  return 0;
}

static int callback_save_parking(void *NotUsed, int argc, char **argv, char **azColName) {
  return 0;
}

static int callback_list_types(void *NotUsed, int argc, char **argv, char **azColName) {
  printf("\033[1m\033[34m%s\033[0m. %s\033[0m\n", argv[0], argv[1]);
  return 0;
}

static int callback_count_types(void *NotUsed, int argc, char **argv, char **azColName) {
  count_types = atoi(argv[0]);
  return 0;
}

static int callback_get_last_vehicle_id(void *NotUsed, int argc, char **argv, char **azColName) {
  last_vehicle_id = atoi(argv[0]);
  return 0;
}

static int _list_types() {
  sqlite3 *dbh;
  char *ErrMsg = 0;
  int con;

  con = sqlite3_open(DB_FILE, &dbh);
  if(con) {
    fprintf(stderr, "\033[31mErreur SQLite3 :: \033[1m%s\033[0m\n", sqlite3_errmsg(dbh));
    exit(1);
  }
  char* Query = "SELECT COUNT(*) AS nb FROM type_vehicules;";
  con = sqlite3_exec(dbh, Query, callback_count_types, 0, &ErrMsg);
  if( con != SQLITE_OK ){
    fprintf(stderr, "\033[31mErreur SQL :: \033[1m%s\033[0m\n", ErrMsg);
    sqlite3_free(ErrMsg);
  }
  if(count_types > 0) {
    char* Query = "SELECT id, libelle FROM type_vehicules ORDER BY id ASC;";
    con = sqlite3_exec(dbh, Query, callback_list_types, 0, &ErrMsg);
    if( con != SQLITE_OK ){
      fprintf(stderr, "\033[31mErreur SQL :: \033[1m%s\033[0m\n", ErrMsg);
      sqlite3_free(ErrMsg);
    } else {
      return 0;
    }
  } else {
    printf("\033[31mErreur :: Vous devez prédéfinir les Types de Véhicule avant de continuer.\033[0m\n");
  }
  return -1;
}

void add_vehicule() {
  sqlite3 *dbh;
  char *ErrMsg = 0;
  int con;
  char matricule[31];
  char id_type_vehicule[11];
  char client[161];
  char date_arrivee[21];

  printf("Types de Véhicules autorisés : \n");
  if(_list_types() == 0) {
    printf("Entrez le \033[1ml'Identifiant du Type de Véhicule\033[0m : ");
    fgets(id_type_vehicule, 10, stdin);
    clear_buffer();
    printf("Entrez le \033[1mMatricule du Véhicule\033[0m : ");
    fgets(matricule, 31, stdin);
    clear_buffer();
    printf("Entrez le \033[1mNom Complet du Client\033[0m : ");
    fgets(client, 160, stdin);
    clear_buffer();
    printf("Entrez la \033[1mDate d'Arrivée\033[0m (Format: AAAA-MM-JJ HH:min): ");
    fgets(date_arrivee, 20, stdin);
    clear_buffer();

    con = sqlite3_open(DB_FILE, &dbh);
    if(con) {
      fprintf(stderr, "\033[31mErreur SQLite3 :: \033[1m%s\033[0m\n", sqlite3_errmsg(dbh));
      exit(1);
    }
    char Query[500];
    sprintf(Query, "INSERT INTO vehicules(id, matricule, id_type_vehicule, client) VALUES(NULL, '%s', '%s', '%s');", trim(matricule), trim(id_type_vehicule), trim(client));
    con = sqlite3_exec(dbh, Query, callback_save_vehicle, 0, &ErrMsg);
    if( con != SQLITE_OK ){
      fprintf(stderr, "\033[31mErreur SQL :: \033[1m%s\033[0m\n", ErrMsg);
      sqlite3_free(ErrMsg);
    } else {
      char* Query2 = "SELECT id FROM vehicules ORDER BY id DESC LIMIT 1;";
      con = sqlite3_exec(dbh, Query2, callback_get_last_vehicle_id, 0, &ErrMsg);
      if( con != SQLITE_OK ){
        fprintf(stderr, "\033[31mErreur SQL :: \033[1m%s\033[0m\n", ErrMsg);
        sqlite3_free(ErrMsg);
      } else {
        // printf("Last vehicle ID :: %d\n", last_vehicle_id);
        char Query3[500];
        sprintf(Query3, "INSERT INTO stationnements(id, id_vehicule, date_arrivee, etat) VALUES(NULL, %d, '%s', 'in');", last_vehicle_id, trim(date_arrivee));
        // printf("Query3 :: %s\n", Query3);
        con = sqlite3_exec(dbh, Query3, callback_save_parking, 0, &ErrMsg);
        if( con != SQLITE_OK ){
          fprintf(stderr, "\033[31mErreur SQL :: \033[1m%s\033[0m\n", ErrMsg);
          sqlite3_free(ErrMsg);
        }
      }
      printf("\033[32mLe Véhicule a été enregistré avec succès !\nSon code de Stationnement est \033[1mP%s-%d\033[0m\n", trim(id_type_vehicule), last_vehicle_id);
    }
  }
  sqlite3_close(dbh);
}
#endif
